<?php

class GoogleSheetsApiService
{
		private $baseUrl = 'https://sheets.googleapis.com/v4';
		private $apiKey;

		public function __construct($apiKey) {
				$this->apiKey = $apiKey;
		}

		public function getContent($sheetId, $tabName, $range) {
				$content = file_get_contents($this->baseUrl . "/spreadsheets/$sheetId/values/$tabName!$range?key=" . $this->apiKey);

				$travels = json_decode($content, true)['values'];
				$keys = array_shift($travels);

				$result = [];
				foreach ($travels as $travel) {

						$tr = [];
						foreach($travel as $i => $attribute) {
								if (!empty($keys[$i])) {
										$this->assignArrayByPath($tr, $keys[$i], $attribute);
								}
						}
						$result[] = $tr;
				}
				return $result;
		}

		private function assignArrayByPath(&$arr, $path, $value, $separator = '.') {
				$keys = explode($separator, $path);

				foreach ($keys as $key) {
						$arr = &$arr[$key];
				}

				$arr = $value;
		}

}


include 'config.php';
