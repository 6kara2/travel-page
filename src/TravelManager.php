<?php

class TravelManager
{
    public function getTravels(array $rawTravels)
    {
        $travels = [];
        foreach ($rawTravels as $rt) {
            $t = new TravelEntity();
            $t->setTravelers(explode(' et ', $rt['travelers']));
            $t->setTransportMode($this->manageTransportMode($rt['transportMode']));
            $t->setStartDate($this->manageDate($rt['date1']));
            $t->setEndDate($this->manageDate($rt['date2']));
            $t->setCountry($this->manageCountry($rt['country']));
            if (!empty($rt['flightsCount'])) {
                $t->setFlightsCount((int)$rt['flightsCount']);
            }
            if (!empty($rt['cities'])) {
                $t->setCities($rt['cities']);
            }
            $travels[] = $t;
        }
        return $travels;
    }

    private function manageTransportMode(string $transportMode)
    {
        if (empty($transportMode)) {
            return '??';
        }

        $choices = [
            'avion' => '✈️',
            'train' => '🚄',
            'voiture' => '🚗',
            'bateau' => '🛥️'
        ];

        return $choices[strtolower($transportMode)];
    }

    private function manageDate(array $date)
    {
        $monthNames = [
            1 => 'janvier',
            2 => 'février',
            3 => 'mars',
            4 => 'avril',
            5 => 'mai',
            6 => 'juin',
            7 => 'juillet',
            8 => 'aout',
            9 => 'septembre',
            10 => 'octobre',
            11 => 'novembre',
            12 => 'décembre'
        ];

        return array_merge($date, [
          'monthName' => empty($date['month']) ? '' : $monthNames[$date['month']]
        ]);
    }

    private function manageCountry(array $country)
    {
        return array_merge($country, [
            'flag' => $this->getCountryFlag($country['iso']),
        ]);
    }

    public function getCountryFlag(string $isoCode)
    {
        return
            dechex(ord($isoCode[0]) % 32 + 0x1F1E5)
            . "-" . dechex(ord($isoCode[1]) % 32 + 0x1F1E5)
            . ".png";
    }
}
