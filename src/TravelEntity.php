<?php

class TravelEntity
{

    private $travelers;
    private $transportMode;
    private $startDate;
    private $endDate;
    private $country;
    private $flightsCount = 0;
    private $cities = [];

    public const TRANSPORT_MODE_PLANE = 'plane';
    public const TRANSPORT_MODE_TRAIN = 'train';
    public const TRANSPORT_MODE_CAR = 'car';
    public const TRANSPORT_MODE_BOAT = 'boat';


    public function getTravelers()
    {
        return $this->travelers;
    }

    public function setTravelers(array $travelers)
    {
        $this->travelers = $travelers;
    }

    public function getTransportMode()
    {
        return $this->transportMode;
    }

    public function setTransportMode(string $transportMode)
    {
        $this->transportMode = $transportMode;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    public function getLength()
    {
        if (empty($this->startDate['year']) || empty($this->startDate['month']) || empty($this->startDate['day'])) {
            return '??';
        }
        $start = new \DateTime($this->startDate['year'] . '-' . $this->startDate['month'] . '-' . $this->startDate['day']);
        $end = new \DateTime($this->endDate['year'] . '-' . $this->endDate['month'] . '-' . $this->endDate['day']);
        $diff = $start->diff($end);
        return $diff->days + 1;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry(array $country)
    {
        $this->country = $country;
    }

    public function getFlightsCount()
    {
        return $this->flightsCount;
    }

    public function setFlightsCount(int $flightsCount)
    {
        $this->flightsCount = $flightsCount;
    }

    public function getCities()
    {
        return $this->cities;
    }

    public function setCities(array $cities)
    {
        $this->cities = $cities;
    }
}
