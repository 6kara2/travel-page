<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/config.php';
require_once __DIR__ . '/src/GoogleSheetsApiService.php';
require_once __DIR__ . '/src/TravelManager.php';
require_once __DIR__ . '/src/TravelEntity.php';

$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/templates');
$twig = new \Twig\Environment($loader, ['debug' => true]);
$twig->addExtension(new \Twig\Extension\DebugExtension());

$gsheetsService = new \GoogleSheetsApiService($apiKey);
$travelManager = new \TravelManager();

$travels = $travelManager->getTravels($gsheetsService->getContent($sheetId, $tabName, $range));

$countriesCount = [];
$flightsCount = 0;
foreach ($travels as $travel) {
    $countriesCount[] = $travel->getCountry()['iso'];
    $flightsCount += $travel->getFlightsCount();
    foreach ($travel->getCities() as $city) {
        if (!empty($city['gps'])) {
          $mapPoints[] = '"' . $city['name'] . '", ' . $city['gps'];
        }
    }
}

try{
    echo $twig->render('page.html', [
      'pageTitle' => $pageTitle,
      'theme' => $theme,
      'stats' => $stats,
      'travels' => $travels,
      'mapPoints' => $mapPoints,
      'countriesCount' => count(array_unique($countriesCount)),
      'flightsCount' => $flightsCount,
    ]);
}
catch (\Exception $e) {
    die($e->getMessage());
}
