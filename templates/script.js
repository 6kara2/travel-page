var map = L.map('map_osm', {
  scrollWheelZoom: false,
  fullscreenControl: {pseudoFullscreen: true}
}).setView([35, 25], 2);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
  minZoom: 1,
  maxZoom: 8
}).addTo(map);

var listeVilles = new Array();
{% for point in mapPoints %}
    listeVilles.push([{{ point|raw }}]);
{% endfor %}

var markers = L.markerClusterGroup({
  singleMarkerMode:true,
  maxClusterRadius:15,
	iconCreateFunction: function (cluster) {
		var childCount = cluster.getChildCount();
		var c = ' marker-cluster-';
		if (childCount < 2) {
			c += 'small';
		} else if (childCount < 5) {
			c += 'medium';
		} else {
			c += 'large';
		}
		return new L.DivIcon({
      html: '<div><span>' + (childCount == 1 ? '⨯' : childCount) + '</span></div>',
      className: 'marker-cluster' + c,
      iconSize: new L.Point(30, 30)
    });
  }
});

for (index = 0; index < listeVilles.length; ++index) {
  var marker = L.marker([listeVilles[index][1], listeVilles[index][2]]);
  marker.bindPopup(listeVilles[index][0]);
  markers.addLayer(marker);
}
map.addLayer(markers);

map.on('blur', function() { map.scrollWheelZoom.disable(); });
map.on('focus', function() { map.scrollWheelZoom.enable(); });

var userList = new List('trips', {valueNames: [{ data: ['id'] }]});
userList.sort('id', { order: "asc" });
